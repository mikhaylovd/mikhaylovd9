import java.util.Scanner;

class MyException extends Exception {
    String word;
    MyException(String word) {this.word = word;}
    public String getMessage(){
        return "Слово " + word + " содержит более 10 букв";
    }
}
class Input {
    static public String input () throws MyException{
        String s = null;
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            String word = null;
            try {
                word = scanner.next();
                s += word;
                if (word.length() > 10) {
                    throw new MyException(word);
                }
            }catch (Exception e){
                System.out.println(e.getMessage());
            }finally {
                if (word.equals("stop")) break;
            }
        }
        return s;
    }
}
public class Main {
    public static void main(String[] args) throws MyException {
        String s = new String();
        s = Input.input();
    }
}
